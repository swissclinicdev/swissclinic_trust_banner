<?php

namespace Swissclinic\TrustBanner\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    protected const XML_PATH_SWISSCLINIC_TRUST_BANNER_CONFIG= 'swissclinic_trust_banner/trust_banner_config/enable';

    /**
     * Data constructor.
     * @param \Magento\Framework\App\Helper\Context $context
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context
    ) {
        parent::__construct($context);
    }

    /**
     * @param null $storeId
     * @return mixed
     */
    public function isEnabled($storeId = null)
    {
        return $this->scopeConfig->isSetFlag(
            self::XML_PATH_SWISSCLINIC_TRUST_BANNER_CONFIG,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

}